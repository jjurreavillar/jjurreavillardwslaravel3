<?php
include_once("Programacion.php");

class Programador extends Programacion
{
    protected $telefonos;

    public function __construct($id, $nombre, $telefonos)
    {
        parent::__construct($id, $nombre);

        $this->telefonos = $telefonos;
    }

    public function getTelefonos()
    {
        return $this->telefonos;
    }
}
?>