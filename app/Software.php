<?php
include_once("Programacion.php");

class Software extends Programacion
{
    protected $programador;

    public function __construct($id, $nombre, $programador)
    {
        parent::__construct($id, $nombre);

        $this->programador = trim($programador);
    }

    public function getProgramador()
    {
        return $this->programador;
    }
}
?>