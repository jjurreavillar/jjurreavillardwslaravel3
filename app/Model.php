<?php
include_once("Programador.php");
include_once("Software.php");

class Model
{
    private $db;

    public function __construct()
    {
        if (USAR_MYSQL != 0)
        {
            $this->Crear("CREATE DATABASE IF NOT EXISTS ceedcv");
            //echo "Creada BDD: ceedcv<br>";
            $this->Crear("CREATE TABLE IF NOT EXISTS programador(
        id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
        nombre VARCHAR(50),
        telefono VARCHAR(25),
        PRIMARY KEY(id)
        )", "ceedcv");
            //echo "Creada tabla: programador<br>";
            $this->Crear("CREATE TABLE IF NOT EXISTS software(
        id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
        pId INTEGER UNSIGNED NOT NULL,
        nombre VARCHAR(50),
        PRIMARY KEY(id),
        FOREIGN KEY (pId) REFERENCES programador(id)
        )", "ceedcv");
            //echo "Creada tabla: software<br>";
            //echo "Creada CAj: software.pId -> programador.id<br>";
            $this->Crear("CREATE TABLE IF NOT EXISTS usuarios(
        nombre VARCHAR(20),
        pass VARCHAR(255),
        PRIMARY KEY (nombre)
        )", "ceedcv");
            //echo "Creada tabla: usuarios<br>";

            // Crear usuario inicial "admin" con contraseña "admin"
            $this->conectarBDD();

            $this->consultarBDD("INSERT IGNORE INTO usuarios(nombre, pass) VALUES ('admin', '" . password_hash("admin", PASSWORD_DEFAULT) . "')");
        }
        else
        {
            $this->db = false;
        }
    }

    public function __destruct()
    {
        if ($this->db !== false && $this->estaConectadaBDD())
            $this->desconectarBDD();
    }

    private function Crear($query, $dbName = null)
    {        
        $conexion = $dbName == null ? 'mysql:host=localhost' : "mysql:host=localhost;dbname=$dbName";

        try
        {
            $pdo = new PDO($conexion, "alumno", "alumno");
            $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);

            $pdo->query($query);

            $pdo = null;
        }
        catch (PDOException $e)
        {
            header("Error grave");
            echo "<p>ERROR: No se ha podido conectar a la base de datos.</p>";
            echo "<br>";
            echo "<p>ERROR: " . $e->getMessage() . "</p>";
            die();
            exit();
        }
    }

    private function estaConectadaBDD()
    {
        if ($this->db !== false)
            return $this->db != null;
    }

    private function conectarBDD()
    {
        try
        {
            $this->db = new PDO("mysql:host=localhost;dbname=ceedcv", "alumno", "alumno");
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
        }
        catch (PDOException $e)
        {
            $this->desconectarBDD();
        }
    }

    private function desconectarBDD()
    {
        if ($this->db !== false)
            $this->db = null;
    }
    
    public function consultarBDD($query)
    {
        if ($this->db !== false)
            return $this->db->query($query);
    }

    public function getProgramadores()
    {
        if ($this->db !== false)
        {
            $filas = $this->db->query("SELECT * FROM programador")->fetchAll(PDO::FETCH_OBJ);

            foreach($filas as $fila)
                $programadores[] = new Programador($fila->id, $fila->nombre, explode(",", $fila->telefono));
        }
        else if (is_file("../app/programadores.txt"))
        {
            $contenidos = file("../app/programadores.txt");
            
            foreach ($contenidos as $linea)
            {
                $linea = trim($linea);
                $arr_programador = explode(";", $linea);
                $arr_telefonos = explode(",", $arr_programador[2]);
                $programadores[] = new Programador($arr_programador[0], $arr_programador[1], $arr_telefonos);
            }
        }
        
        return $programadores;
    }
    
    public function getSoftware()
    {        
        if ($this->db !== false)
        {
            $filas = $this->db->query("SELECT * FROM software")->fetchAll(PDO::FETCH_OBJ);
            
            foreach($filas as $fila)
                $softwares[] = new Software($fila->id, $fila->nombre, $fila->pId);
        }
        else if (is_file("../app/software.txt"))
        {
            $listaSw = file("../app/software.txt");

            foreach ($listaSw as $sw)
            {
                $campos = explode(";", $sw);
                
                $softwares[] = new Software($campos[0], $campos[1], $campos[2]);
            }
        }
        
        if (isset($softwares))
            return $softwares;
    }
    
    public function insertarProgramador($programador)
    {
        if ($this->db !== false)
            $this->consultarBDD("INSERT INTO programador(id, nombre, telefono) VALUES ('".
                               $programador->getId() . "', '" .
                               $programador->getNombre() . "', '" .
                               implode(",", $programador->getTelefonos()) .
                               "')");
        else
        {
            $f = fopen("../app/programadores.txt", "at");
            fwrite($f, $programador->getId() . ';' . $programador->getNombre() . ';' . implode(",", $programador->getTelefonos()) . "\n");
            fclose($f);
        }
    }
    
    public function insertarSoftware($software)
    {
        if ($this->db !== false)
        {
            $this->consultarBDD("INSERT INTO software(id, nombre, pId) VALUES ('".
                                $software->getId() . "', '" .
                                $software->getNombre() . "', '" .
                                $software->getProgramador() .
                                "')");
        }
        else
        {
            $f = fopen("../app/software.txt", "at");
            fwrite($f, $software->getId() . ';' . $software->getNombre() . ';' . $software->getProgramador() . "\n");
            fclose($f); 
        }
    }
    
    public function verificarUsuario($usuario, $pass)
    {
        if ($this->db !== false)
        {
            $consulta = $this->consultarBDD("SELECT * FROM usuarios");

            foreach($consulta as $valor)
            {
                $nombreUsuarios[] = $valor['nombre'];
                $passUsuarios[] = $valor['pass'];
            }
        }
        else
        {
            if (is_file("../app/usuarios.txt"))
            {
                $contenidos = file("../app/usuarios.txt");
                foreach ($contenidos as $indice => $linea)
                {
                    $valorLinea = explode("=", $linea, 2);

                    if ($indice % 2 == 0)
                        $nombreUsuarios[] = trim($valorLinea[1]);
                    else
                        $passUsuarios[] = trim($valorLinea[1]);
                }
            }
            else // Si el archivo usuarios.txt, lo creamos con el usuario por defecto admin y contraseña admin
            {
                $nombreUsuarios[] = "admin";
                $passUsuarios[] = password_hash("admin", PASSWORD_DEFAULT);

                $usuariosArc = fopen("../app/usuarios.txt", "wt");
                fwrite($usuariosArc, "nombre=$nombreUsuarios[0]\n");
                fwrite($usuariosArc, "pass=$passUsuarios[0]\n");
                fclose($usuariosArc);
            }
        }
        
        $indice = array_search($usuario, $nombreUsuarios);
        
        if ($indice !== false && password_verify($pass, $passUsuarios[$indice]))
        {
            $_SESSION["nombre"] = $nombreUsuarios[$indice];
            $_SESSION["autorizado"] = true;

            // Habiendo verificado el inicio de sesión, cambiamos la ID de sesión para reducir el riesgo de ataques por suplantación
            session_regenerate_id();
        }
    }
}
?>
