<?php
class Programacion
{
    protected $id;
    protected $nombre;

    public function __construct($id, $nombre)
    {
        $this->id = trim($id);
        $this->nombre = trim($nombre);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
}
?>