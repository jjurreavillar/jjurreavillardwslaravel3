<?php ob_start() ?>
    <h2>Inicie sesión</h2>
    <div id="subcontenedor">
        <form action="{{ url('/') }}" method="POST">
            <table id="login">
                <tr>
                    <td>Usuario</td>
                    <td><input type="text" name="nombre"></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="text" name="pass"></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Enviar" name="login"></td>
                    <td><input type="reset" value="Borrar"></td>
                </tr>
            </table>
        </form>
<?php
if (isset($_POST['nombre']) && (!isset($_SESSION["autorizado"]) || !$_SESSION["autorizado"]))
    echo "<p>Error: Usuario/Password incorrecto</p>";
?>
    </div>
<?php $articulo = ob_get_clean() ?>

@include('layout')
