<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Proyecto</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="css/general.css">
        <link rel="stylesheet" href="css/principal.css">
    </head>
    <body>
        <header>
<?php if (isset($_SESSION["autorizado"]) && $_SESSION["autorizado"]): ?>

    <label id="saludo">Hola, {{ $_SESSION["nombre"] }}</label>
    <a id="cerrarSesion" href="{{ url('/cerrar') }}">Haz clic aquí para cerrar la sesión</a>
<?php endif ?>
            <h1>{{ TITULO }}</h1>
            <h3>Proyecto UD11 DWC</h3>
        </header>
        <div id="contenedor">
            <article>
<?= $articulo ?>
            </article>
<?php if (isset($menu)): ?>
	<?= $menu ?>
<?php endif ?>
        </div>
    </body>
</html>
