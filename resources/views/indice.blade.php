<?php
include_once(__DIR__ . '/../../../app/Model.php');
if (isset($_POST['login']) && isset($_POST['nombre']) && isset($_POST['pass']))
{
    $nombreEnviado = htmlspecialchars(trim(strip_tags($_POST['nombre'])));
    $passEnviado = htmlspecialchars(trim(strip_tags($_POST['pass'])));

    $m = new Model();
    $m->verificarUsuario($nombreEnviado, $passEnviado);
    unset($m);
}
?>
<?php if (!isset($_SESSION["autorizado"]) || !$_SESSION["autorizado"]): ?>
<script type="text/javascript">
    window.location = "{{ url('/login') }}";
</script>
<?php endif ?>

<?php ob_start() ?>
<h2>Elija una opción:</h2>
<div id="subcontenedor">
    <div id="programador" class="opcion"><a href="{{ url('/insertarP') }}">Programador</a></div>
    <div id="software" class="opcion"><a href="{{ url('/insertarS') }}">Software</a></div>
</div>
<?php $articulo = ob_get_clean() ?>

<?php ob_start() ?>
<?php
$m = new Model();
$softwares = $m->getSoftware();
unset($m);

if (isset($softwares) && count($softwares) > 0)
{

    echo "<aside><nav><p>Software disponible:</p><ul>";
    
    foreach ($softwares as $software)
        echo '<li><a href="{{ url(' . "'ver'", '[id=' . $software->getId() . ']) }}' . '" title="' . $software->getNombre() . '">' . $software->getNombre() . '</a></li>';
    
    echo "</ul></nav></aside>";
}
?>
<?php $menu = ob_get_clean() ?>

@include('layout')
