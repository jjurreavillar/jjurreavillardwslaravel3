<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('indice');
});

Route::get('/', function () {
    return view('login');
});

Route::get('/cerrar', function () {
    return view('cerrar');
});

Route::get('insertarP', function () {
    return view('insertarP');
});

Route::get('insertarS', function () {
    return view('insertarS');
});

Route::get('ver', function () {
    return view('ver');
});

